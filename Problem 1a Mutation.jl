'Mutate an allele with PMutation, count number of mutations
Function Mutation(Alleleval As Boolean, PMutation As Single, NMutation As Integer)
As Boolean
Dim Mutate As Boolean
Mutate = Flip(PMutation)
If Mutate Then
NMutation = NMutation + 1
Mutation = Not Alleleval
Else
Mutation = Alleleval
End If
End Function
