Sub InitPop()
Dim j As Integer
Dim j1 As Integer
For j = 1 To PopSize
With OldPop(j)
For j1 = 1 To lchrom
.Chromosome(j1) = Flip(0.5)
Next j1
.x = Decode(.Chromosome, lchrom) 'decode the string
.Fitness = ObjFunc(.x) 'evaluate initial fitness
.Parent1 = 0
.Parent2 = 0
.XSite = 0
End With
Next j
End Sub
