Gen = 0
Initialize( OldPop )
Do
Gen = Gen + 1
Generation( OldPop, NewPop )
For ii = 1 To PopSize
OldPop(ii) = NewPop(ii) 'advance the generation
Next ii
Loop Until ( (Gen > MaxGen) or (MaxFitness > DesiredFitness) )

PopSize = 30 'population size
lchrom = 30 'chromosome length
MaxGen = 10
PCross = 0.6
PMutation = 0.0333
ReDim GenStat(1 To (MaxGen + 1))
'Initialize random number generator
Randomize
'Initialize counters
NMutation = 0
NCross = 0
